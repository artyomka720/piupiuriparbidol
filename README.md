# Jistkefet

How to install:

----


* Clone Repo
```
sudo git clone https://gitlab.com/artyomka720/kiskefet.git
```

* Go to the repository directory
```
...  cd kiskefet
```

* Install dependencies
```bash
npm install
```
* Copy setup example
```bash
cp setup.json.example setup.json
```
* Edit it and put correct database login data:
```bash
nano setup.json
```


How to run:

----

* Get start dev
```
npm start dev
```

